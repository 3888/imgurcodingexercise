package core.ui;

import android.support.annotation.StringRes;

public interface MvpView {
    void showMessage(@StringRes int messageResId);

    void showMessage(String messageMsg);
}
