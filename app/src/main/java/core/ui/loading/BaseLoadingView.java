package core.ui.loading;


import core.ui.MvpView;

public interface BaseLoadingView extends MvpView {

    void showLoading();

    void hideLoading();
}
