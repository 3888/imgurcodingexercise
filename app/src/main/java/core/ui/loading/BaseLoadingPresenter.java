package core.ui.loading;


import core.ui.BasePresenter;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseLoadingPresenter<V extends BaseLoadingView> extends BasePresenter<V> {

    private CompositeDisposable disposables = new CompositeDisposable();

    protected void addDisposable(Disposable disposable) {
        if (this.disposables == null) {
            synchronized (this) {
                if (this.disposables == null) {
                    this.disposables = new CompositeDisposable();
                }
            }
        }
        this.disposables.add(disposable);
    }

    protected void dispose() {
        if (disposables != null) {
            disposables.dispose();
        }
    }


    @Override
    public void detachView() {
        disposables.clear();
        super.detachView();
    }


}