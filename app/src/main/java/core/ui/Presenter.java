package core.ui;

interface Presenter<V> {
    void attachView(V view);

    void detachView();
}
