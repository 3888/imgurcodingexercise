package com.coding.exercise.imgur.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.coding.exercise.imgur.BuildConfig;
import com.coding.exercise.imgur.R;
import com.coding.exercise.imgur.di.AppContext;
import com.coding.exercise.imgur.network.ServerApi;
import com.coding.exercise.imgur.network.response.UploadImage;
import com.coding.exercise.imgur.ui.main.common.BaseMvpPresenter;
import com.coding.exercise.imgur.ui.main.gallery.GalleryItemModel;
import com.coding.exercise.imgur.util.GalleryUtils;
import com.coding.exercise.imgur.util.RxCursorIterable;

import java.util.ArrayList;

import javax.inject.Inject;

import io.paperdb.Paper;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.coding.exercise.imgur.database.PaperStorage.LINK_LIST;

public class MainPresenter extends BaseMvpPresenter<MainMvpView> {

    static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 123;
    private static final String TAG = MainPresenter.class.getSimpleName();

    private Context context;
    private ArrayList<String> linkList;

    @Inject
    ServerApi serverApi;

    @Inject
    MainPresenter(@AppContext Context context) {
        this.context = context;
        this.linkList = Paper.book().read(LINK_LIST, new ArrayList<>());
    }

    void getImagesFromGallery() {
        Cursor cursor = GalleryUtils.getCursor(context);
        ArrayList<GalleryItemModel> result = new ArrayList<>(cursor.getCount());

        final Disposable disposable = Observable.fromIterable(RxCursorIterable.from(cursor))
                .doAfterNext(cursor12 -> {
                    if (cursor12.getPosition() == cursor12.getCount() - 1) {
                        cursor12.close();
                    }
                })
                .doOnSubscribe(disposable1 -> {
                })
                .subscribe(cursor1 -> {
                    final int dataColumn = cursor1.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    final int nameColumn = cursor1.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);

                    GalleryItemModel galleryItemModel = new GalleryItemModel(
                            cursor1.getString(dataColumn), cursor1.getString(nameColumn));
                    result.add(galleryItemModel);

                }, throwable -> Timber.tag(TAG).e(throwable));
        addDisposable(disposable);
        mvpView.initAdapter(result);
    }

    void uploadImage(MultipartBody.Part file, int itemPosition) {
        updateItemUploadProgress(itemPosition);
        serverApi.uploadImage(file).enqueue(new Callback<UploadImage>() {
            @Override
            public void onResponse(Call<UploadImage> call, Response<UploadImage> response) {
                assert response.body() != null;
                mvpView.showMessage(context.getString(R.string.main_upload_successful, response.body().getData().getId()));

                linkList.add(BuildConfig.IMGUR_URL.concat(response.body().getData().getId()));
                Paper.book().write(LINK_LIST, linkList);

                updateItemUploadProgress(itemPosition);
            }

            @Override
            public void onFailure(Call<UploadImage> call, Throwable t) {
                mvpView.showMessage(t.getMessage());
                updateItemUploadProgress(itemPosition);
            }
        });
    }

    void openUploadedFile(int position) {
        String url = linkList.get(position);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);
    }

    boolean isPermissionsGranted() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    void requestPermissions(Activity context) {
        ActivityCompat.requestPermissions(context,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
    }

    private void updateItemUploadProgress(int itemPosition) {
        mvpView.uploadItemProgress(itemPosition);
    }
}