package com.coding.exercise.imgur.di.module;

import android.app.Activity;

import dagger.Module;

@Module
public class ViewModule {

    protected final Activity activity;

    public ViewModule(Activity activity) {
        this.activity = activity;
    }
}
