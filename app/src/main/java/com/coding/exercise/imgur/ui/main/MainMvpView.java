package com.coding.exercise.imgur.ui.main;

import com.coding.exercise.imgur.ui.main.gallery.GalleryItemModel;

import java.util.List;

import core.ui.loading.BaseLoadingView;

public interface MainMvpView extends BaseLoadingView {

    void initAdapter(List<GalleryItemModel> galleryItemModels);

    void uploadItemProgress(int itemPosition);

}
