package com.coding.exercise.imgur.di.component;



import com.coding.exercise.imgur.di.module.AppModule;
import com.coding.exercise.imgur.di.module.DataModule;
import com.coding.exercise.imgur.di.module.ViewModule;


import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
})
public interface AppComponent {

    ViewComponent plus(ViewModule module);

}