package com.coding.exercise.imgur.network;

import com.coding.exercise.imgur.BuildConfig;
import com.coding.exercise.imgur.network.response.UploadImage;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Server Api interface
 * <p>
 */
public interface RetrofitRestService {

    @Multipart
    @Headers({"Authorization: " + BuildConfig.CLIENTID})
    @POST("3/image")
    Call<UploadImage> postImage(@Part MultipartBody.Part file);

}