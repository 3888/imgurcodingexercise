package com.coding.exercise.imgur.ui.main.common;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.coding.exercise.imgur.AndroidApp;
import com.coding.exercise.imgur.di.component.ViewComponent;
import com.coding.exercise.imgur.di.module.ViewModule;

import javax.inject.Inject;

import core.ui.BaseActivity;
import core.ui.BasePresenter;

public abstract class BaseMvpActivity<P extends BasePresenter> extends BaseActivity {

    @Inject
    protected P presenter;

    protected abstract void inject();

    public ViewComponent getViewComponent() {
        AndroidApp app = (AndroidApp) getApplication();
        return app.getComponent()
                .plus(new ViewModule(this));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        initPresenter(savedInstanceState);
    }

    protected void initPresenter(Bundle savedInstanceState) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle argumentsBundle = getIntent().getExtras();
            if (savedInstanceState != null) {
                argumentsBundle.putAll(savedInstanceState);
            }
            presenter.initFromBundle(argumentsBundle);
        }

        if (savedInstanceState != null) {
            presenter.onRestoreInstanceState(savedInstanceState);
        }

        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }


}
