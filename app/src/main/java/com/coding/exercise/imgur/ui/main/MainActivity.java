package com.coding.exercise.imgur.ui.main;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.coding.exercise.imgur.R;
import com.coding.exercise.imgur.ui.main.common.BaseMvpActivity;
import com.coding.exercise.imgur.ui.main.gallery.GalleryAdapter;
import com.coding.exercise.imgur.ui.main.gallery.GalleryItemModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.paperdb.Paper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.coding.exercise.imgur.database.PaperStorage.LINK_LIST;
import static com.coding.exercise.imgur.ui.main.MainPresenter.MY_PERMISSIONS_REQUEST_READ_CONTACTS;

public class MainActivity extends BaseMvpActivity<MainPresenter>
        implements MainMvpView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.galleryList)
    RecyclerView recyclerViewGallery;

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int SPAN_PORTRAIT_COUNT = 3;
    private static final int SPAN_LANDSCAPE_COUNT = 5;

    private GalleryAdapter adapter;

    @Override
    protected void inject() {
        getViewComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter.isPermissionsGranted()) {
            presenter.getImagesFromGallery();
        } else {
            presenter.requestPermissions(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        if (item.getItemId() == R.id.menuLinks) {
            Dialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.main_links_dialog_title);

            ListView list = new ListView(this);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, android.R.id.text1,
                    Paper.book().read(LINK_LIST, new ArrayList<>()));
            list.setAdapter(adapter);
            list.setOnItemClickListener((adapterView, view, i, l) ->
                    presenter.openUploadedFile(i));

            builder.setView(list);
            dialog = builder.create();
            dialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
            for (int i = 0; i < permissions.length; i++) {
                int grantResult = grantResults[i];
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    presenter.getImagesFromGallery();
                } else {
                    showMessage(R.string.main_permissions_error);
                }
            }
        }
    }

    @Override
    public void initAdapter(List<GalleryItemModel> galleryItemModels) {
        adapter = new GalleryAdapter(this, galleryItemModels, (file, position) -> {
            if (file == null) {
                showMessage(R.string.main_choose_file_error);
                adapter.updateProgress(position);
                adapter.updateProgress(position);
                return;
            }

            presenter.uploadImage(MultipartBody.Part.createFormData(
                    "image", file.getName(),
                    RequestBody.create(MediaType.parse("image/*"), file)), position);

        });

        recyclerViewGallery.setLayoutManager(new GridLayoutManager(this,
                getResources().getConfiguration().orientation ==
                        Configuration.ORIENTATION_PORTRAIT ? SPAN_PORTRAIT_COUNT : SPAN_LANDSCAPE_COUNT));
        recyclerViewGallery.setAdapter(adapter);
        adapter.setItems(galleryItemModels);
    }

    @Override
    public void uploadItemProgress(int itemPosition) {
        adapter.updateProgress(itemPosition);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}