package com.coding.exercise.imgur.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UploadImage {

    public Data getData() {
        return data;
    }

    @SerializedName("data")
    private Data data;

    @SerializedName("success")
    private boolean success;

    @SerializedName("status")
    private int status;


    public class Data {

        @SerializedName("in_most_viral")
        private boolean inMostViral;

        @SerializedName("ad_type")
        private int adType;

        @SerializedName("link")
        private String link;

        @SerializedName("description")
        private Object description;

        @SerializedName("section")
        private Object section;

        @SerializedName("title")
        private Object title;

        @SerializedName("type")
        private String type;

        @SerializedName("deletehash")
        private String deletehash;

        @SerializedName("datetime")
        private int datetime;

        @SerializedName("has_sound")
        private boolean hasSound;

        public String getId() {
            return id;
        }

        @SerializedName("id")
        private String id;

        @SerializedName("in_gallery")
        private boolean inGallery;

        @SerializedName("vote")
        private Object vote;

        @SerializedName("views")
        private int views;

        @SerializedName("height")
        private int height;

        @SerializedName("bandwidth")
        private int bandwidth;

        @SerializedName("nsfw")
        private Object nsfw;

        @SerializedName("is_ad")
        private boolean isAd;

        @SerializedName("edited")
        private String edited;

        @SerializedName("ad_url")
        private String adUrl;

        @SerializedName("tags")
        private List<Object> tags;

        @SerializedName("account_id")
        private int accountId;

        @SerializedName("size")
        private int size;

        @SerializedName("width")
        private int width;

        @SerializedName("account_url")
        private Object accountUrl;

        @SerializedName("name")
        private String name;

        @SerializedName("animated")
        private boolean animated;

        @SerializedName("favorite")
        private boolean favorite;
    }
}