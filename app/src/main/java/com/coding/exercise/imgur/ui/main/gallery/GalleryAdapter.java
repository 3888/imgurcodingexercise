package com.coding.exercise.imgur.ui.main.gallery;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.coding.exercise.imgur.R;
import com.coding.exercise.imgur.util.ScreenUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import core.adapter.BaseRecyclerAdapter;
import core.adapter.BaseRecyclerHolder;

public class GalleryAdapter extends BaseRecyclerAdapter<GalleryItemModel, GalleryAdapter.GalleryViewHolder> {

    private final ClickListener clickListener;

    public GalleryAdapter(Context context, List<GalleryItemModel> items, ClickListener clickListener) {
        super(context);
        setItems(items);

        this.clickListener = clickListener;
    }



    public void updateProgress(int position) {
        getItem(position).setSelected(!getItem(position).isSelected);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GalleryViewHolder(inflater.inflate(R.layout.item_custom_row_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        GalleryItemModel currentItem = getItem(position);
        File imageSelected = new File(currentItem.imageUri);

        Picasso.get()
                .load(imageSelected)
                .centerCrop()
                .resize(ScreenUtils.getScreenWidth(context, 2), ScreenUtils.getScreenHeight(context, 3))
                .into(holder.image);

        holder.progress.setVisibility(currentItem.isSelected ? View.VISIBLE : View.INVISIBLE);
        holder.image.setOnClickListener(view ->
                clickListener.onItemSelected(imageSelected, position));
    }


    public interface ClickListener {
        void onItemSelected(File file, int position);
    }

    static class GalleryViewHolder extends BaseRecyclerHolder {
        @BindView(R.id.item_image)
        ImageView image;
        @BindView(R.id.item_imageProgress)
        ProgressBar progress;

        GalleryViewHolder(View view) {
            super(view);
        }
    }
}
