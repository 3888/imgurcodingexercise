package com.coding.exercise.imgur.di.module;

import android.content.Context;

import com.coding.exercise.imgur.BuildConfig;
import com.coding.exercise.imgur.di.AppContext;
import com.coding.exercise.imgur.network.AppServerApi;
import com.coding.exercise.imgur.network.RetrofitRestService;
import com.coding.exercise.imgur.network.ServerApi;
import com.parkingwang.okhttp3.LogInterceptor.LogInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {
    private final String baseUrl;

    public DataModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    OkHttpClient provideOkHttpClient(@AppContext Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            LogInterceptor logInterceptor = new LogInterceptor();
            builder.addInterceptor(logInterceptor);
        }

        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofitClient(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    RetrofitRestService provideRetrofitRestService(Retrofit retrofit) {
        return retrofit.create(RetrofitRestService.class);
    }

    @Provides
    @Singleton
    ServerApi provideRetrofitRestApi(RetrofitRestService restService) {
        return new AppServerApi(restService);
    }
}
