package com.coding.exercise.imgur.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;


public class ScreenUtils {

    public static int getScreenWidth(Context context, int divider) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels / divider;
    }

    public static int getScreenHeight(Context context, int divider) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels / divider;
    }
}
