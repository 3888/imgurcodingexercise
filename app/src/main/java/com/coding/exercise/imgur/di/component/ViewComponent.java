package com.coding.exercise.imgur.di.component;


import com.coding.exercise.imgur.di.ViewScope;
import com.coding.exercise.imgur.di.module.ViewModule;
import com.coding.exercise.imgur.ui.main.MainActivity;

import dagger.Subcomponent;

@ViewScope
@Subcomponent(modules = {ViewModule.class})
public interface ViewComponent {

    void inject(MainActivity activity);

}
