package com.coding.exercise.imgur;

import android.app.Application;

import com.coding.exercise.imgur.di.component.AppComponent;
import com.coding.exercise.imgur.di.component.DaggerAppComponent;
import com.coding.exercise.imgur.di.module.AppModule;
import com.coding.exercise.imgur.di.module.DataModule;

import io.paperdb.Paper;
import timber.log.Timber;

public class AndroidApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        Paper.init(this);
    }

    public AppComponent getComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .dataModule(new DataModule(BuildConfig.API))
                    .build();
        }
        return appComponent;
    }
}