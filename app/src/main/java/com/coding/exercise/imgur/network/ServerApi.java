package com.coding.exercise.imgur.network;


import com.coding.exercise.imgur.network.response.UploadImage;

import okhttp3.MultipartBody;
import retrofit2.Call;

public interface ServerApi {

    Call<UploadImage> uploadImage(MultipartBody.Part file);

}