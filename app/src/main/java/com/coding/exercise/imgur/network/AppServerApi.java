package com.coding.exercise.imgur.network;


import com.coding.exercise.imgur.network.response.UploadImage;

import okhttp3.MultipartBody;
import retrofit2.Call;

public class AppServerApi implements ServerApi {

    private final RetrofitRestService apiService;

    public AppServerApi(RetrofitRestService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Call<UploadImage> uploadImage(MultipartBody.Part file) {
        return apiService.postImage(file);
    }
}