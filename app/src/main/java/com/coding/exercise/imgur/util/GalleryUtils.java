package com.coding.exercise.imgur.util;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.provider.MediaStore;


public class GalleryUtils {

    private static final String USER_DEVICE_GALLERY_PATH
            = Environment.getExternalStorageDirectory().toString().concat("/").concat(android.os.Environment.DIRECTORY_DCIM);

    private static String getBucketId(String path) {
        return String.valueOf(path.toLowerCase().hashCode());
    }

    public static Cursor getCursor(Context context) {
        final String[] projection = {MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA};
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = {GalleryUtils.getBucketId(USER_DEVICE_GALLERY_PATH)};

        return context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);

    }
}